---
name: whstack-mariadb-v10-8-vm
provider:
  name: kvm
bootstrapper:
  workspace: /target/os-images
  mirror: https://debian.packages.managed-infra.com/debian/
  include_packages:
   - initramfs-tools
   - linux-headers-amd64
   - whois
   - psmisc
   - apt-transport-https
   - ca-certificates
   - ssl-cert
system:
  release: bullseye
  architecture: amd64
  hostname: whstack-mariadb-v10-8-vm
  bootloader: grub
  charmap: UTF-8
  locale: en_US
  timezone: Europe/Berlin
volume:
  backing: qcow2
  partitions:
    type: gpt
    boot:
      filesystem: ext4
      size: 1GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
    swap:
      size: 2GiB
      mountopts:
        - sw
        - discard
        - noatime
        - errors=remount-ro
    root:
      filesystem: ext4
      size: 7GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
    var/log:
      filesystem: ext4
      size: 5GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
packages:
  install:
    # custom standard packages
    - chrony
    - unp
    - locate
    - lsscsi
    - sg3-utils
    - unattended-upgrades
    - debsecan
    - vim
    - nano
    - vim-nox
    - unzip
    - rsync
    - davfs2
    - bzip2
    - zstd
    - binutils
    - sudo
    - anacron
    - screen
    - tree
    - zram-tools
    - haveged
    - needrestart
    - rng-tools
    - resolvconf
    - wget
    - curl
    # network tools
    - ethtool
    - wpasupplicant
    - mstflint
    - iproute2
    - dante-client
    - proxychains
    - proxychains4
    - stunnel4
    - net-tools
    - mtr
    - traceroute
    - ebtables
    - tcpdump
    - snmpd
    - snmp
    - snmp-mibs-downloader
    - lldpd
    - ipxe
    - ipxe-qemu
    - memtest86+
    # backup
    - borgbackup
    - proxmox-backup-client
    - etckeeper
    # email smarthost
    - dma
    - mailutils
    - mutt
    # cloud specific packages
    - cloud-init
    - salt-minion
    - puppet-agent
    - chef
    - qemu-guest-agent
    - open-vm-tools
    # user access management (directory)
    #- freeipa-client # missing in bullseye
    - sssd
    - sssd-dbus
    - libsss-sudo
    - libsss-idmap0
    - libsss-nss-idmap0
    - krb5-config
    - krb5-k5tls
    - krb5-user
    # - libpam-krb5
    - libpam-ccreds
    # monitoring
    - lm-sensors
    - nvme-cli
    - apcupsd
    - smartmontools
    - monit
    - icinga2
    - lynis
    - trivy
    - fio
    - zabbix-agent
    #- omi # missing in bullseye
    #- scx # missing in bullseye
    - watchdog
    - monitoring-plugins
    - monitoring-plugins-basic
    - monitoring-plugins-standard
    - monitoring-plugins-btrfs
    - nagios-plugins-contrib
    - nagios-snmp-plugins
    - nagios-plugins-rabbitmq
    - nagios-check-xmppng
    - libmonitoring-plugin-perl
    - libipc-run-perl
    - liblist-moreutils-perl
    - filebeat
    - metricbeat
    - auditbeat
    - heartbeat-elastic
    - packetbeat
    # security
    - cracklib-runtime
    - wazuh-agent
    - auditd
    - audispd-plugins
    - ipset
    - conntrack
    - shorewall
    - shorewall6
    - rkhunter
    - fail2ban
    - crowdsec
    - crowdsec-firewall-bouncer-iptables
    # system management
    - aptitude
    - rpm
    - apt-file
    - apt-show-versions
    - apt-listchanges
    - debsums
    - atop
    - iotop
    - nload
    - neofetch
    # MariaDB
    - mariadb-common
    - mariadb-client
    - mariadb-server
    - mariadb-backup
    - percona-toolkit
    - galera-4
    - lz4
    - libmariadb3
    - nginx-full
    - nginx-extras
    - libnginx-mod-http-brotli
    - adminer
    - composer
    - yarn
    # cluster tools
    - pacemaker
    - heartbeat
    - corosync
    - keepalived
    - exabgp
    - python3
    - python3-dev
    - python3-venv
    - python3-pip
    - python3-setuptools
    - python3-wheel
    # storage packages
    - multipath-tools
    - ceph
    - rbd-nbd
    - glusterfs-client
    # web stats
    - awstats
    - goaccess
    - geoip-database
    - libclass-dbi-mysql-perl
    - libtimedate-perl
    # php 8.1 support
    - php8.1
    - php8.1-bcmath
    - php8.1-bz2
    - php8.1-cgi
    - php8.1-cli
    - php8.1-common
    - php8.1-curl
    - php8.1-dba
    - php8.1-dev
    - php8.1-enchant
    - php8.1-fpm
    - php8.1-gd
    - php8.1-gmp
    - php8.1-imap
    - php8.1-interbase
    - php8.1-intl
    - php8.1-ldap
    - php8.1-mbstring
    - php8.1-mysql
    - php8.1-odbc
    - php8.1-opcache
    - php8.1-pgsql
    - php8.1-phpdbg
    - php8.1-pspell
    - php8.1-raphf
    - php8.1-readline
    - php8.1-snmp
    - php8.1-soap
    - php8.1-sqlite3
    - php8.1-sybase
    - php8.1-tidy
    - php8.1-xml
    - php8.1-xsl
    - php8.1-zip
    # pomerium
    - pomerium
    - pomerium-cli
    # administration
    #- webmin
  install_standard: false
  mirror: https://debian.packages.managed-infra.com/debian/
  security: https://debian-security.packages.managed-infra.com/
  sources:
    debian-backports:
      - deb https://debian.packages.managed-infra.com/debian bullseye-backports main contrib non-free
    debian-fasttrack:
      - deb https://debian-fasttrack.packages.managed-infra.com/debian/ bullseye-fasttrack main contrib
      # - deb https://debian-fasttrack.packages.managed-infra.com/debian/ bullseye-backports main contrib
    pbs-client:
      - deb https://proxmox.packages.managed-infra.com/debian/pbs-client bullseye main
    icinga:
      - deb https://icinga.packages.managed-infra.com/debian icinga-bullseye main
    zabbix:
      - deb https://zabbix.packages.managed-infra.com/zabbix/6.5/debian bullseye main
      - deb-src https://zabbix.packages.managed-infra.com/zabbix/6.5/debian bullseye main
    microsoft:
      - deb https://microsoft.packages.managed-infra.com/debian/11/prod bullseye main
    aquasecurity:
      - deb https://aquasecurity.packages.managed-infra.com/trivy-repo/deb bullseye main
    crowdsec:
      - deb https://packagecloud.packages.managed-infra.com/crowdsec/crowdsec/any/ any main
    newrelic-infra:
      - deb https://newrelic.packages.managed-infra.com/infrastructure_agent/linux/apt bullseye main
    webmin:
      - deb https://webmin.packages.managed-infra.com/download/repository sarge contrib
    elasticsearch:
      - deb https://elastic.packages.managed-infra.com/packages/8.x/apt stable main
    puppet:
      - deb https://puppetlabs-apt.packages.managed-infra.com bullseye puppet
    chef:
      - deb https://chef.packages.managed-infra.com/repos/apt/stable bullseye main
    ceph:
      - deb https://croit.packages.managed-infra.com/debian-quincy/ bullseye main
    mariadb:
      - deb https://mariadb.packages.managed-infra.com/repo/10.8/debian bullseye main
      - deb-src https://mariadb.packages.managed-infra.com/repo/10.8/debian bullseye main
      - deb https://mariadb-downloads.packages.managed-infra.com/Tools/debian bullseye main
    percona:
      - deb https://percona.packages.managed-infra.com/tools/apt bullseye main
    yarnpkg:
      - deb https://yarnpkg.packages.managed-infra.com/debian/ stable main
    goaccess:
      - deb https://goaccess.packages.managed-infra.com/ bullseye main
    suryorg:
      - deb https://sury.packages.managed-infra.com/nginx-mainline/ bullseye main
      - deb https://sury.packages.managed-infra.com/php/ bullseye main
    pomerium:
      - deb https://cloudsmith.packages.managed-infra.com/public/pomerium/pomerium/deb/debian bullseye main
      - deb-src https://cloudsmith.packages.managed-infra.com/public/pomerium/pomerium/deb/debian bullseye main
    gluster:
      - deb https://gluster.packages.managed-infra.com/pub/gluster/glusterfs/LATEST/Debian/bullseye/amd64/apt bullseye main
    saltstack:
      - deb https://saltstack.packages.managed-infra.com/artifactory/saltproject-deb/ stable main
    wazuh:
      - deb https://wazuh.packages.managed-infra.com/4.x/apt/ stable main
    cisofy:
      - deb https://cisofy.packages.managed-infra.com/community/lynis/deb/ stable main
    falcosecurity:
      - deb https://falco.packages.managed-infra.com/packages/deb stable main
  components:
    - main
    - contrib
    - non-free
  trusted-keys:
    - trusted-repo-keys/trusted-repo-keys.gpg
  apt.conf.d:
    00InstallRecommends: >-
      APT::Install-Recommends "false";
      APT::Install-Suggests   "false";
    00RetryDownload: 'APT::Acquire::Retries "3";'
plugins:
  tmpfs_workspace: {}
  admin_user:
    username: admin
    password: ch4ng3m3
    pubkey: ../authorized_keys
  apt_proxy:
    address: 127.0.0.1
    port: 8000
  debconf: >-
    d-i pkgsel/install-language-support boolean false
    popularity-contest popularity-contest/participate boolean false
    resolvconf resolvconf/linkify-resolvconf boolean true
  file_copy:
    files:
      - { src: ../copy-to-filesystem.tar.gz,
          dst: /opt/copy-to-filesystem.tar.gz,
          permissions: -rwxrwxrwx,
          owner: root,
          group: root }
  commands:
    commands:
      - ['chroot {root} tar --strip-components 1 -xvzf /opt/copy-to-filesystem.tar.gz -C /']
      - ['chroot {root} chmod 770 /usr/sbin/cmdb-deploy']
      - ['chroot {root} chmod 770 /usr/bin/cleanup_image']
      - ['chroot {root} /usr/bin/cleanup_image']
  minimize_size:
    zerofree: true
    apt:
      autoclean: true
